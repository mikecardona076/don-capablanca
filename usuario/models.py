from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Usuario(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    #///////////////////////////////////////////////////////////////////////
    nombre_usr = models.CharField(max_length=20,default='Nombre')
    #///////////////////////////////////////////////////////////////////////
    primer_apellido = models.CharField(max_length=30,default='Apellidos')
    #///////////////////////////////////////////////////////////////////////
    fecha_nacimiento = models.DateField(blank=True)
    #///////////////////////////////////////////////////////////////////////
    lugar_procedencia = models.CharField(max_length=50,default='Lugar de procedencia')
    #///////////////////////////////////////////////////////////////////////
    correo_usr = models.EmailField(max_length=254, default='Correo@ejemplo.com')
    #///////////////////////////////////////////////////////////////////////
    foto_perfil = models.ImageField(blank=True, upload_to='media')

    def __str__(self):
        return '{}'.format(self.user)

class FichaUsuario(models.Model):
        user = models.OneToOneField(User, on_delete=models.CASCADE)
        #///////////////////////////////////////////////////////////////////////
        titulo_usr = models.CharField(max_length=50)
        #///////////////////////////////////////////////////////////////////////
        nivel_elo = models.IntegerField()
        def __str__(self):
            return '{}'.format(self.user)

class Publicaciones(models.Model):
        user = models.ForeignKey(User, on_delete=models.CASCADE)
        #///////////////////////////////////////////////////////////////////////
        titulo_publ = models.CharField(max_length=50)
        #///////////////////////////////////////////////////////////////////////
        cuerpo_publ = models.TextField()
        #///////////////////////////////////////////////////////////////////////
        imagen_ayuda = models.ImageField(blank=True)

        def __str__(self):
            return '{}'.format(self.user)

class Quejas_Sugerencias(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    motivo = models.CharField(max_length=20)
    queja = models.TextField()
