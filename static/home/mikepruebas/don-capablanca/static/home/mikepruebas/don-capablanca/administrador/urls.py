from django.urls import path, include
from . import views
from .views import *
from usuario.models import Usuario
from django.conf.urls import url
# from django.contrib.auth.views import LogoutView
# from django.conf import settings

app_name  = 'administrador'

urlpatterns =[
    #///////////////////////////////////////////////////////////////////////
    path('', views.presentacion , name='presentacion'),
    #///////////////////////////////////////////////////////////////////////
    path('nosotros/', views.nosotros , name='nosotros'),
    #///////////////////////////////////////////////////////////////////////
    path('registro/', views.registro,  name='registro'),
    #///////////////////////////////////////////////////////////////////////
    path('tienda_chess/', views.tienda_chess , name='tienda_chess'), #CAMBIAR
    #///////////////////////////////////////////////////////////////////////
    path('configuraciones/', views.configuraciones, name='configuraciones'),
    #///////////////////////////////////////////////////////////////////////
    path('crear_usuario/', views.CrearUsuario.as_view(), name='CrearUsuario'),
    #///////////////////////////////////////////////////////////////////////
    path('logout_cuenta/', views.logout_cuenta, name='logout_cuenta'),
    #///////////////////////////////////////////////////////////////////////
    path('publicaciones/', views.publicaciones, name='publicaciones'),
    #///////////////////////////////////////////////////////////////////////
    path('0006/<int:pk>', views.Editar_usuario.as_view(), name="Editar_usuario"),
    #///////////////////////////////////////////////////////////////////////
    path('llenar_info/', views.llenar_info, name='llenar_info'),
    #///////////////////////////////////////////////////////////////////////
    path('registro_formularios/', views.registro_formularios, name='registro_formularios'),
    #///////////////////////////////////////////////////////////////////////
    path('Tienda/', views.Tienda, name='Tienda'),
    #///////////////////////////////////////////////////////////////////////
    path('perfil_usuario/', views.perfil_usuario, name='perfil_usuario'),
    #///////////////////////////////////////////////////////////////////////
    
    #///////////////////////////////////////////////////////////////////////
    path('Ver_publicacion/<int:pk>', views.Ver_publicaciones.as_view(), name="Ver_publicaciones"), 
    path('Ver_publicaciones_Comunidad/<int:pk>', views.Ver_publicaciones_Comunidad.as_view(), name="Ver_publicaciones_Comunidad"), 
    path('Editar_publicacion<int:pk>', views.Editar_publicaciones.as_view(), name="Editar_publicaciones"),
    path('Eliminar_publicacion<int:pk>', views.Eliminar_publicaciones.as_view(), name="Eliminar_publicaciones"),
    #///////////////////////////////////////////////////////////////////////


]
