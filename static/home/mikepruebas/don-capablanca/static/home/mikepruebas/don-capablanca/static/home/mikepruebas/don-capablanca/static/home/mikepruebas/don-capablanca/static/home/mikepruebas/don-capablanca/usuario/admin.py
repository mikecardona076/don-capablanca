from django.contrib import admin
from .models import Usuario, FichaUsuario, Publicaciones, Quejas_Sugerencias

# Register your models here.
admin.site.register(Usuario)
admin.site.register(FichaUsuario)
admin.site.register(Publicaciones)
admin.site.register(Quejas_Sugerencias)
