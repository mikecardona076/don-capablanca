
Django==3.0.5

Pillow==7.0.0

django-filter==2.3.0

django-simple-captcha==0.5.12

django-ranged-response==0.2.0
