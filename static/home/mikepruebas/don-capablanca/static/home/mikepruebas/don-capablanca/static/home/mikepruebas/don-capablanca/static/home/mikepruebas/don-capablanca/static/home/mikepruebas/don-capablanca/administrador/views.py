from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth import authenticate, login, logout 
from django.core.files import File 
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .forms import LoginForm, NewUser
from usuario.models import Usuario, Publicaciones, FichaUsuario
from .models import Producto
from usuario.forms import NuevoUsuario, NuevaFicha, NuevaPublicacion, Nuevaqueja
from django.contrib import messages
from django.views.generic import UpdateView, ListView, FormView
from django.contrib.auth import REDIRECT_FIELD_NAME
from .filters import ChessFilters
from captcha.models import CaptchaStore
from captcha.helpers import captcha_image_url
import pdb


# Create your views here.
#///////////////////////////////////////////////////////////////////////
''' Aqui esta reflejado como es que trabaja el sistema '''
def presentacion(request): #Funcion presentacion
    context = {

    }
    return render(request, 'Inicio/index.html', context)


#///////////////////////////////////////////////////////////////////////
def nosotros(request):
    context = {

    }
    return render(request, 'Inicio/about.html', context)


#///////////////////////////////////////////////////////////////////////
def tienda_chess(request):

    #INFORMACION DE USUARIOS
    info_usuarios = Usuario.objects.filter(user = request.user )
    
    #PUBLICACIONES
    publicaciones_usuarios = Publicaciones.objects.filter()

    if request.method == 'POST':
        form = Nuevaqueja(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()
            form.save()
            return redirect('administrador:tienda_chess')
    else:
        form = Nuevaqueja()
    context = {
        'info_usuarios':info_usuarios,
        'form':form,
        'publicaciones_usuarios' : publicaciones_usuarios,
    }
    return render(request, 'Tienda/index.html', context)

#///////////////////////////////////////////////////////////////////////
def registro(request): 
    message = 'Admin MACC'
    form = LoginForm(request.POST or None)
    if request.method == 'POST':
        form = LoginForm(request.POST or None)
        if form.is_valid():
            user = request.POST['username']
            password = request.POST['password']
            human = True
            print(user, password)
            user = authenticate(username= user, password = password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('administrador:tienda_chess')
                    message = 'user Loged' 
                else:
                    message = '  Sólo para administradores '
            else:
                message = 'Usuario o contraseña incorrecta'
        context ={
        'form': form, 
        'message': message
        }
        
    context ={
        'form': form,
        'message': message
    }
    return render(request, 'cuenta_usuario/iniciar_cuenta.html', context)

#///////////////////////////////////////////////////////////////////////
def publicaciones(request):
    if request.method == 'POST':
        form = NuevaPublicacion(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()
            form.save()
            return redirect('administrador:tienda_chess')
    else:
        form = NuevaPublicacion()
    return render(request, 'usuario/publicaciones.html', {'form':form } )


#///////////////////////////////////////////////////////////////////////

def perfil_usuario(request):

    #INFORMACIÓN DEL USUARIO 
    Usuario_profile = Usuario.objects.filter(user = request.user )
    FichaUsuario_profile = FichaUsuario.objects.filter(user = request.user )
    Publicaciones_profile = Publicaciones.objects.filter(user = request.user )
    Pic_user = Usuario.objects.get(user=request.user)

    context ={
        'Usuario_profile' : Usuario_profile,
        'FichaUsuario_profile' : FichaUsuario_profile,
        'Publicaciones_profile' : Publicaciones_profile,
        'Pic_user' : Pic_user
    }

    return render(request,'usuario/perfil_usuario.html', context)

#///////////////////////////////////////////////////////////////////////


class CrearUsuario(generic.FormView): 
    template_name = 'cuenta_usuario/crear_cuenta.html'
    form_class = NewUser
    success_url = reverse_lazy('administrador:registro_formularios')

    def form_valid(self, form):
        user = form.save()
        form.save()
        return super(CrearUsuario, self).form_valid(form)


#///////////////////////////////////////////////////////////////////////
def logout_cuenta(request):
    logout(request)
    context ={

    }
    return render(request,'cuenta_usuario/salir_cuenta.html', context)

#///////////////////////////////////////////////////////////////////////
class Editar_usuario(UpdateView):
    template_name = "usuario/informacion_usuario.html"
    model = Usuario
    form_class = NuevoUsuario
    success_url = reverse_lazy('administrador:tienda_chess')



#///////////////////////////////////////////////////////////////////////
@login_required(login_url='administrador:registro_formularios' )
def llenar_info(request):
    if request.method == 'POST':
        form = NuevoUsuario(request.POST or None , request.FILES or None)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()
            form.save()
            return redirect('administrador:configuraciones')
    else:
        form = NuevoUsuario()
    return render(request, 'usuario/informacion_usuario.html', {'form':form } )

#///////////////////////////////////////////////////////////////////////

@login_required(login_url='administrador:registro_formularios' )
def configuraciones(request):
    if request.method == 'POST':
        form = NuevaFicha(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.user = request.user
            instance.save()
            form.save()
            return redirect('administrador:tienda_chess')
    else:
        form = NuevaFicha()
    return render(request, 'usuario/ficha_usuario.html', {'form':form } )

#///////////////////////////////////////////////////////////////////////

def registro_formularios(request):
    message = 'Admin MACC'
    form = LoginForm(request.POST or None)
    if request.method == 'POST':
        form = LoginForm(request.POST or None)
        if form.is_valid():
            user = request.POST['username']
            password = request.POST['password']
            print(user, password)
            user = authenticate(username= user, password = password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('administrador:llenar_info')
                    message = 'user Loged' 
                else:
                    message = '  Sólo para administradores '
            else:
                message = 'Usuario o contraseña incorrecta'
        context ={
        'form': form, 
        'message': message
        }
        
    context ={
        'form': form,
        'message': message
    }
    return render(request, 'cuenta_usuario/iniciar_cuenta.html', context)


#///////////////////////////////////////////////////////////////////////
#Tienda 

def Tienda(request):
    #FILTERS
    busqueda_productos = ChessFilters(request.GET, queryset=Producto.objects.all())

    #PRODUCTOS EN LA BASE DE DATOS
    productos_publicados = Producto.objects.filter()

    context = {
        'productos_publicados' : productos_publicados,
        'busqueda_productos' : busqueda_productos,
    }

    return render(request, 'Tienda/tienda.html', context)



#///////////////////////////////////////////////////////////////////////
#Publicaciones   VER, EDITAR Y ELIMINAR 

class Ver_publicaciones(LoginRequiredMixin, generic.DetailView):# Ver
    template_name = 'Publicaciones_acciones/ver.html'
    model = Publicaciones

class Ver_publicaciones_Comunidad(LoginRequiredMixin, generic.DetailView):# Ver
    template_name = 'Tienda/ver_publicaciones.html'
    model = Publicaciones

class Editar_publicaciones(LoginRequiredMixin, generic.UpdateView):# EDitar 
    template_name = 'Publicaciones_acciones/editar.html'
    model = Publicaciones
    form_class = NuevaPublicacion
    success_url = reverse_lazy('administrador:perfil_usuario')

class Eliminar_publicaciones(LoginRequiredMixin, generic.DeleteView):# Eliminar
    template_name = 'Publicaciones_acciones/eliminar.html'
    model = Publicaciones
    success_url = reverse_lazy('administrador:perfil_usuario')

#///////////////////////////////////////////////////////////////////////