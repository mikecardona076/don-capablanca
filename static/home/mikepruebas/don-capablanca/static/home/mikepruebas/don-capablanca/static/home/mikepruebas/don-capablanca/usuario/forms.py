from django import forms 
from administrador.views import *
from .models import Usuario, FichaUsuario, Publicaciones, Quejas_Sugerencias
from django.contrib.auth.forms import UserCreationForm
from django.forms.widgets import Textarea
# from django.urls import reverse_lazy

# from django.contrib.auth.forms import UserCreationFor

#///////////////////////////////////////////////////////////////////////
class NuevoUsuario(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = [
            'nombre_usr',
            'primer_apellido',
            'fecha_nacimiento',
            'lugar_procedencia',
            'correo_usr',
            'foto_perfil'
        ]
        labels = {
            'nombre_usr':'Nombre',
            'primer_apellido': 'Apellidos',
            'fecha_nacimiento':'Fecha de Nacimiento',
            'lugar_procedencia': 'Lugar de procedencia',
            'correo_usr':'Correo',
            'foto_perfil' : 'Foto de perfil'
        }
        widgets = {
            'nombre_usr':forms.TextInput(attrs={'class':"input100"}),
            'primer_apellido':forms.TextInput(attrs={'class':"input100"}),
            'fecha_nacimiento':forms.TextInput(attrs={'class':"input100",'type':'date'}),
            'lugar_procedencia': forms.TextInput(attrs={'class':"input100"}),
            'correo_usr':forms.TextInput(attrs={'class':"input100"}),
            }

        
        def clean_field(self):
            print( self.cleaned_data )
            return self.cleaned_data
            
#///////////////////////////////////////////////////////////////////////


class NuevaFicha(forms.ModelForm):
    class Meta:
        model = FichaUsuario
        fields = [
            'titulo_usr',
            'nivel_elo'
        ]
        labels = {
            'titulo_usr' : 'Titulo',
            'nivel_elo' : 'Nivel de Elo'
        }
        widgets = {
            'titulo_usr' : forms.TextInput(attrs={'class':"input100"}),
            'nivel_elo' : forms.TextInput(attrs={'class':"input100"})
        }
#///////////////////////////////////////////////////////////////////////

class NuevaPublicacion(forms.ModelForm):
    class Meta:
        model = Publicaciones
        fields = [
            'titulo_publ',
            'cuerpo_publ',
            'imagen_ayuda'
        ]
        labels = {
            'titulo_publ' : 'Titulo de publicacion',
            'cuerpo_publ' : 'Contenido',
            'imagen_ayuda' : 'Imagen de ejemplo'
        }
        widgets = {
            'titulo_publ' : forms.TextInput(attrs={}),
            'cuerpo_publ' : Textarea(attrs={'cols': 80, 'rows': 10}),
            'imagen_ayuda' : forms.TextInput(attrs={'type':'file', 'accept':'image/*'})
        }

class Nuevaqueja(forms.ModelForm):
        class Meta:
            model = Quejas_Sugerencias
            fields = [
                'motivo',
                'queja'
            ]
            labels = {
                'motivo' : 'Motivo',
                'queja' : 'Queja o Sugerencia'
            }
            widgets = {
                'motivo' : forms.TextInput(attrs={}),
                'queja' : Textarea(attrs={'cols': 80, 'rows': 10})
            }
