from django import forms
from . import views
from django.contrib.auth.forms import UserCreationForm
from captcha.fields import CaptchaField


class LoginForm(forms.Form):
    #Con estas lineas de código podemos registrar a un usuario
    username = forms.CharField(widget=forms.TextInput(attrs={'class':"input100"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':"input100"}))
    captcha = CaptchaField() 


class NewUser(UserCreationForm):
    correo = forms.CharField(max_length=20)
