from django.db import models
# from djmoney.models.fields import MoneyField

# Create your models here.

#ESTE ES EL MODELO DEL PRODUCTO
''' FACILMENTE SE PUEDE MODIFICAR
RECORDAR QUE TODAS LAS IMAGENES SE GUARDAN EN LA DB Y EN EL ARCHIVO MEDIA DE ESTE PROYECTO
'''
class Producto(models.Model):
    tipo_producto = models.CharField(max_length=30)
    imagen_producto = models.ImageField()
    descipcion_producto = models.TextField()
    # precio = MoneyField(max_digits=14, decimal_places=2, default_currency='USD')